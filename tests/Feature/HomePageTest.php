<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;


class HomePageTest extends TestCase
{


    //Позволяет обойти мидлвер
   // use WithoutMiddleware;




    /**
     * A basic test example.
     *
     * @return void
     *
     * vendor\bin\phpunit
     */


    public function testHomepage()
    {

//        Session::start();
//        $response = $this->call('POST', route('login'), [
//            'email' => 'votoziz@gmail.com',
//            'password' => '100500Qq',
//            '_token' => csrf_token()
//        ]);
//
//
////        $view = $response->getContent();
////        dd($view);
//
////        dd($response->original->name());
//        $this->followingRedirects();
//        $response->assertStatus(200);


        //WORK рабочая программа логина
        $response = $this->followingRedirects()
            ->post(route('login'), [
                'email' => 'votoziz@gmail.com',
                'password' => '100500Qq',
                '_token' => csrf_token()
            ])
            ->assertStatus(200);
        //После логина можно переходить дальше на другие страницы в тесте

        $response = $this->get(route('profiles.index',[1]));

        $response->assertStatus(200);

//                $view = $response->getContent();
//                  dd($view);

        //Получает имя роута в который перекинуло
        //        dd($response->original->name());

    }

    public function testUsersPage()
    {
//
//        $user = new User(array('email' => 'votoziz@gmail.com', 'password' => '100500Qq', '_token' => csrf_token()));
//        $this->be($user);
//
//
//        $response = $this->get(route('profiles.index',[1]));
//
//                        $view = $response->getContent();
//                  dd($view);
//
//
//        $response->assertStatus(200);

        $response = $this->get('/');

        $response->assertStatus(302);
    }
}
