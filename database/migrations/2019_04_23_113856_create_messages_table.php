<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('message_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });


        DB::table('message_types')->insert([
            'name' => 'Личное сообщение',
            'slug' => 'private',
        ]);
        DB::table('message_types')->insert([
            'name' => 'Публичное сообщение',
            'slug' => 'public',
        ]);
        DB::table('message_types')->insert([
            'name' => 'Запрос',
            'slug' => 'request',
        ]);
        DB::table('message_types')->insert([
            'name' => 'Системное',
            'slug' => 'system',
        ]);




        DB::table('message_statuses')->insert([
            'name' => 'Черновик',
            'slug' => 'draft',
        ]);
        DB::table('message_statuses')->insert([
            'name' => 'Отправлено',
            'slug' => 'sent',
        ]);
        DB::table('message_statuses')->insert([
            'name' => 'Просмотрено',
            'slug' => 'seen',
        ]);
        DB::table('message_statuses')->insert([
            'name' => 'Удалено у отправителя',
            'slug' => 'deleted_sender',
        ]);
        DB::table('message_statuses')->insert([
            'name' => 'Удалено у получателя',
            'slug' => 'deleted_recipient',
        ]);

        DB::table('message_statuses')->insert([
            'name' => 'Удалено',
            'slug' => 'deleted',
        ]);




        Schema::create('message_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->comment('Заголовок сообщения');
            $table->text('text')->comment('само сообщение');

            $table->integer('message_type_id')->unsigned()->default(1)->comment('Обязательно указывается тип сообщения');
            $table->foreign('message_type_id')->references('id')->on('message_types')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });



        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');


            //При создании черновика получатель и отправитель это один и тот же человек, сообщение замыкается на себя.
            $table->integer('from_user_id')->unsigned()->comment('отправитель сообщения');
            $table->foreign('from_user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');


            $table->integer('to_user_id')->unsigned()->comment('получатель сообщения');
            $table->foreign('to_user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');



            $table->integer('message_data_id')->unsigned()->comment('Само сообщение');
            $table->foreign('message_data_id')->references('id')->on('message_data')
                ->onUpdate('cascade')->onDelete('cascade');



            $table->integer('message_status_id')->unsigned()->default(1)->comment('Обязательно указывается статус сообщения');
            $table->foreign('message_status_id')->references('id')->on('message_statuses')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
        Schema::dropIfExists('message_data');
        Schema::dropIfExists('message_types');
        Schema::dropIfExists('message_statuses');
    }
}
