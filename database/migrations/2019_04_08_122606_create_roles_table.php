<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });


//
//        Schema::create('role_users', function (Blueprint $table) {
//            $table->integer('user_id')->unsigned();
//            $table->integer('role_id')->unsigned();
//            $table->nullableTimestamps();
//            $table->primary(['user_id', 'role_id']);
//
//            $table->foreign('user_id')->references('id')->on('users')
//                ->onUpdate('cascade')->onDelete('cascade');
//
//            $table->foreign('role_id')->references('id')->on('roles')
//                ->onUpdate('cascade')->onDelete('cascade');
//
//        });


        DB::table('roles')->insert([
            'name' => 'owner',
        ]);
        DB::table('roles')->insert([
            'name' => 'admin',
        ]);
        DB::table('roles')->insert([
            'name' => 'moder',
        ]);
        DB::table('roles')->insert([
            'name' => 'user',
        ]);

//        DB::table('role_users')->insert([
//            'user_id' => '1',
//            'role_id' => '1',
//
//        ]);

        Schema::table('users', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->nullable()->after('email_verified_at');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');
        });


        DB::table('users')->insert([
            'name' => 'Votoziz',
            'email' => 'votoziz@gmail.com',
            'password' => Hash::make('100500Qq'),
            'role_id' => 1,
        ]);


    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_role_id_foreign');
            $table->dropColumn('role_id');
        });

//        Schema::dropIfExists('role_users');
        Schema::dropIfExists('roles');
    }
}
