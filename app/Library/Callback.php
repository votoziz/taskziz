<?php
namespace App\Library;

use App\Poll_status;
use App\Poll_type;
use App\Qu_status;
use App\Qu_type;

class Callback {

    //Статичные функции вызываемые для уменьшения кода
    public static function translate_qu()
    {
        //Перевод статуса вопросов
        $question_statuses = Qu_status::pluck('name', 'id')->toarray();
        $data['qu_statuses'] = [];
        for ($i = 1; $i <= count($question_statuses); $i++) { // расчет с 1 и до длинны запрашиваемого массива
            $data['qu_statuses'][$i] = trans('incontroller.' . $question_statuses[$i]);// Перевод запроса через файл incontroller.php
        }
        //Перевод типов
        $question_types = Qu_type::pluck('name', 'id')->toarray();
        $data['qu_types'] = [];
        for ($i = 1; $i <= count($question_types); $i++) { // расчет с 1 и до длинны запрашиваемого массива
            $data['qu_types'][$i] = trans('incontroller.' . $question_types[$i]);// Перевод запроса через файл incontroller.php
        }

        return ($data);
    }
    //Перевод типа опросов
    public static function translate_poll()
    {
        $poll_types = Poll_type::pluck('name','id')->toarray();//определяет выборку как массив
        $data['poll_types'] = [];//это то что будет отправленно на сервер
        for ($i = 1; $i <= count($poll_types); $i++) { // расчет с 1 и до длинны запрашиваемого массива
            $data['poll_types'][$i] = trans('incontroller.'.$poll_types[$i]);// Перевод запроса через файл incontroller.php
        }
        //Выбор статусов из базы, используя перевод запроса.
        $statuses = Poll_status::pluck('name','id')->toarray();//определяет выборку как массив
        $data['poll_statuses'] = [];//это то что будет отправленно на сервер
        for ($i = 1; $i <= count($statuses); $i++) { // расчет с 1 и до длинны запрашиваемого массива
            $data['poll_statuses'][$i] = trans('incontroller.'.$statuses[$i]);// Перевод запроса через файл incontroller.php
        }
        return ($data);
    }



    public static function generateString($length = 8){
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }
//Вот так вызывать
//$data = array_merge($data, Callback::translate_poll());

}