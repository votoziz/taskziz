<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    public $table = "messages";

    protected $fillable = [
        'from_user_id', 'to_user_id', 'message_data_id', 'message_status_id'
    ];
}
