<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Role extends Model implements RoleInterface
{
    public $table = "roles";

    protected $fillable = [
        'name',
    ];

    protected static $usersModel = 'App\User';

    /*ФУНКЦИИ для интерфейса**********************************************/
    public function getRoleId()
    {
        return $this->getKey();
    }

    public function getRoleSlug()
    {
        return $this->name;
    }

    /**
     * Returns all users for the role.
     *
     * @return \IteratorAggregate
     */
    public function getUsers()
    {
        // TODO: Implement getUsers() method.
    }

    /**
     * Returns the users model.
     *
     * @return string
     */
    public static function getUsersModel()
    {
        // TODO: Implement getUsersModel() method.
    }

    /**
     * Sets the users model.
     *
     * @param  string $usersModel
     * @return void
     */
    public static function setUsersModel($usersModel)
    {
        // TODO: Implement setUsersModel() method.
    }

    /**********************************************************************/




    //Получить всех пользователей
    public function users()
    {
        return $this->hasMany(static::$usersModel);
    }


}
