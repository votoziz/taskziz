<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    //Теперь примари кеем будет это значение
    protected $primaryKey = 'user_id';


    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'second_name', 'phone'
    ];
}
