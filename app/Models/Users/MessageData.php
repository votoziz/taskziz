<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class MessageData extends Model
{


    public $table = "message_data";

    protected $fillable = [
        'title', 'text', 'message_type_id'
    ];
}
