<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function __construct()
    {
        $this->middleware('auth');


        $bom = 'test';
        // Sharing is caring
        View::share('test', $bom);
    }



    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $data['user'] = $user;

        // 4 varables true or false = $is_owner $is_admin $is_moder $is_user
        $data = array_merge($data, $user->getRoleVariables());

        return view('home',$data);
    }


    public function testpage()
    {
        $user = Auth::user();
        $data['user'] = $user;

        // 4 varables true or false = $is_owner $is_admin $is_moder $is_user
        $data = array_merge($data, $user->getRoleVariables());

        return view('testpage',$data);
    }




    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}
