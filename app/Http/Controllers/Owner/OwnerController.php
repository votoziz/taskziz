<?php

namespace App\Http\Controllers\Owner;

use App\Models\Users\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class OwnerController extends Controller
{

    public function __construct()
    {
        //Данный контроллер может использовать только Owner
        $this->middleware('role:owner');

    }



    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    *
    * Все пользователи
    *
    * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public function all_users(Request $request)
    {



        $request = $request->all();
        $like = $request['like'] ?? null;
        if (is_null($like)){
            $data['users'] = $users = User::paginate(2);
        }else{
            $data['users'] = $users = User::where('email','like','%'.$like.'%')->paginate(2);
        }


        return view('owner.all_users',$data);
    }


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     * Кнопки по изменению роли
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public function change_role($user_id, $role_id, $current_page)
    {
        $user = User::find($user_id);
        $role_id = ($role_id == 0) ? null : $role_id;
        $user->role_id = $role_id;
        $user->save();

        return redirect()->route('all_users','page='.$current_page);
    }
}
