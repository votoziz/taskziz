<?php

namespace App\Http\Controllers\Owner;

use App\Models\Users\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Users\Role  $roles
     * @return \Illuminate\Http\Response
     */
    public function show(Role $roles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Users\Role  $roles
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $roles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Users\Role  $roles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $roles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Users\Role  $roles
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $roles)
    {
        //
    }
}
