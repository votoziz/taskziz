<?php

namespace App\Http\Controllers\Api;

use App\Http\Traits\MessageTrait;
use App\Models\Users\Message;
use App\Models\Users\MessageData;
use App\Models\Users\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ApiMessageController extends Controller
{
    //Здесь остались только приватные методы
    use MessageTrait;


    /*******************************************************
     * Как всё это работает?
     * Здесь мы обращаемся к трейту и получаем из него данные в виде массива, затем преобразуем в json с необходимым статусом
     *
     ************/

    /**
     * Получение всех писем определенного типа, где kind_of: sent -  отправленные, received - полученные, draft - черновики
     * Входящие параметры: id пользователя системы, тип сообщений
     * Исходящие параметры: json
     */

    public function index($user_id, $kind_of = 'received')
    {

        $data['user'] = $user = User::find($user_id);
        $data['kind_of'] = $kind_of;
        //сколько новых входящих
        $data['received_new_count'] = Message::where('to_user_id', $user_id)->where('message_status_id', 2)->count();
        //сколько записей на странице
        $paginate = 4;

        if ($kind_of == 'received') {
            $whereInMessageStatus = [2, 3, 4];
            $toOrFromUser = 'to_user_id';
            $senderOrReceiversMessagesPluck = 'from_user_id';
        } elseif ($kind_of == 'sent') {
            $whereInMessageStatus = [2, 3, 5];
            $toOrFromUser = 'from_user_id';
            $senderOrReceiversMessagesPluck = 'to_user_id';
        } elseif ($kind_of == 'draft') {
            $whereInMessageStatus = [1];
            $toOrFromUser = 'from_user_id';
            $senderOrReceiversMessagesPluck = 'to_user_id';
        }
        $messages = Message::where($toOrFromUser, $user_id)->whereIn('message_status_id', $whereInMessageStatus)->orderBy('id', 'DESC')->paginate($paginate);

        if (is_null($messages) || count($messages) == 0) {
            $data['messages'] = null;
        } else {
            $data = array_merge($this->get_messages_pagination($messages), $data);
            $sendersOrReceivers_id = array_unique($messages->pluck($senderOrReceiversMessagesPluck)->toArray());
            $sendersOrReceivers = User::whereIn('id', $sendersOrReceivers_id)->get();
            $messages_data_id = array_unique($messages->pluck('message_data_id')->toArray());
            $messages_data = MessageData::whereIn('id', $messages_data_id)->get();
            $supermass = [];
            foreach ($messages as $message):
                $message_id = $message->id;
                $supermass[$message_id] = [];
                $supermass[$message_id]['id'] = $message_id;
                $supermass[$message_id]['to_or_from'] = $sendersOrReceivers->find($message->$senderOrReceiversMessagesPluck)->name ?? $sendersOrReceivers->find($message->$senderOrReceiversMessagesPluck)->email;
                $supermass[$message_id]['message_status_id'] = $message->message_status_id;
                $supermass[$message_id]['title'] = $messages_data->find($message->message_data_id)->title ?? mb_substr($messages_data->find($message->message_data_id)->text, 0, 30) . '...';
            endforeach;
            //todo сделть так, чтбы непрочитанные сообщения всегда были выше тек, которые уже прочитаны
            $data['messages'] = $supermass;
        }


        return response()->json($data, 200);
    }

    /**
     * Чтение письма
     * Входящие параметры: id письма
     * Исходящие параметры: json
     */

    public function read($message_id, $first_time_read = false)
    {
        $message = Message::find($message_id);

        if (is_null($message)) {
            return response()->json(['err' => 'Not Found'], 404);
        }
        if ($first_time_read) {
            $message->message_status_id = 3;
            $message->save();
        }
        $message_data = MessageData::find($message->message_data_id);
        $from_user = User::find($message->from_user_id);
        $to_user = User::find($message->to_user_id);
        $data['id'] = $message_id;
        $data['title'] = $message_data->title;
        $data['text'] = $message_data->text;
        //Дата в простом формате обычно не передается по api
        $data['created_at'] = date('d.m.Y', strtotime($message_data->created_at));
        $data['from_name'] = $from_user->name;
        $data['from_email'] = $from_user->email;
        $data['to_name'] = $to_user->name;
        $data['to_email'] = $to_user->email;
        $data['message_status_id'] = $message->message_status_id;


        return response()->json($data, 200);

    }


    /**
     * Чтение письма
     * Входящие параметры: id письма
     * Исходящие параметры: json
     */

    public function search_users(Request $request)
    {
        $method = $request->method();
        $input = $request->all();


        $last_name = $input['last_name'];
        $name = $input['name'];
        $email = $input['email'];

        if (!is_null($last_name)) {
            $profiles = Profile::where('last_name', 'like', '%' . $last_name . '%')->get();
            $profile_users_id = $profiles->pluck('user_id')->toArray();

        } else {
            $profiles = null;
            $profile_users_id = null;
        }

        if (is_null($profile_users_id)) {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->get();
        } else {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->whereIn('id', $profile_users_id)->get();
        }

        $users_id = $users->pluck('id')->toArray();

        $result = [];

        foreach ($users as $user) {

            $result[$user->id] = [];
            $result[$user->id]['id'] = $user->id;
            $result[$user->id]['email'] = $user->email;
            $result[$user->id]['name'] = $user->name;

        }


        return response()->json($result, 200);
    }


    public function store(Request $request)
    {
        $input = $request->all();
        //Проверка на черновик
        (isset($input['draft']) && $input['draft']) ? $draft = true : $draft = false;
        ($draft) ? $return_message = 'Сообщение сохранено как черновик' : $return_message = 'Сообщение отправлено';
        ($draft) ? $message_status = 1 : $message_status = 2;
        //Если иного не указано тип сообщения 1 = Личное сообщение
        (isset($input['message_type_id'])) ? $message_type_id = $input['message_type_id'] : $message_type_id = 1;

        $from_user_id = $input['from'];
        $title = $input['title'];
        $text = $input['text'];
        $to_users = $input['to_users'];

        //create new data
        $message_data = MessageData::create([
            'title' => $title,
            'text' => $text,
            'message_type_id' => $message_type_id,
        ]);

        if ($draft) {
            $message = Message::create([
                'from_user_id' => $from_user_id,
                'to_user_id' => $from_user_id,
                'message_data_id' => $message_data->id,
                'message_status_id' => $message_status,
            ]);
        } else {
            foreach ($to_users as $user):
                $message = Message::create([
                    'from_user_id' => $from_user_id,
                    'to_user_id' => $user['id'],
                    'message_data_id' => $message_data->id,
                    'message_status_id' => $message_status,
                ]);
            endforeach;
        }

        return response()->json($input, 200);
    }


///////////////////////////////////////////////////////пока не рабочие методы/////////////////////////////////////////////////////////////


    public function create()
    {

        $data['user'] = $user = Auth::user();


        return view('messages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    //todo при создании сообщения можно сохранить его как черновик, тогода в получателя пишется отправитель
    //todo при создании сообщений группе людей дублировать сообщения меняя получателя
    public function asdasd(Request $request)
    {
        $input = $request->all();

        //act 1 chek is it a draft
        (isset($input['draft']) && $input['draft']) ? $draft = true : $draft = false;
        //act 1.1 create return message
        ($draft) ? $return_message = 'Сообщение сохранено как черновик' : $return_message = 'Сообщение отправлено';
        //act 1.2 default message status
        ($draft) ? $message_status = 1 : $message_status = 2;
        //act 2 set default message type
        (isset($input['message_type_id'])) ? $message_type_id = $input['message_type_id'] : $message_type_id = 1;

        //act 3 set other varables
        $user_id = $input['user_id'];
        $title = $input['title'];
        $text = $input['text'];
        $users = $input['users'];

        //act 4 create message data

        $message_data = MessageData::create([
            'title' => $title,
            'text' => $text,
            'message_type_id' => $message_type_id,
        ]);

        //act 5 create message

        foreach ($users as $user):

            $message = Message::create([
                'from_user_id' => $user_id,
                'to_user_id' => $user,
                'message_data_id' => $message_data->id,
                'message_status_id' => $message_status,
            ]);

        endforeach;


        return redirect()->route('messages.index', $user_id)->with('success', $return_message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        $data['message'] = $message;

        $users_id = array_unique(Message::where('message_data_id', $message->message_data_id)->pluck('to_user_id')->toArray());

        $users = User::whereIn('id', $users_id)->get();

        $data['users'] = [];
        $data['selected_users'] = [];

        foreach ($users as $sender):

            $data['users'][$sender->id] = $sender->name . ' ' . $sender->email;
            $data['selected_users'][$sender->id] = $sender->id;

        endforeach;

        $data['message_data'] = MessageData::find($message->message_data_id);

        return view('messages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        $input = $request->all();

        //act 1 chek is it a draft
        (isset($input['draft']) && $input['draft']) ? $draft = true : $draft = false;
        //act 1.1 create return message
        ($draft) ? $return_message = 'Сообщение сохранено как черновик' : $return_message = 'Сообщение отправлено';
        //act 1.2 default message status
        ($draft) ? $message_status = 1 : $message_status = 2;
        //act 2 set default message type
        (isset($input['message_type_id'])) ? $message_type_id = $input['message_type_id'] : $message_type_id = 1;

        //act 3 set other varables

        $title = $input['title'];
        $text = $input['text'];
        $users = $input['users'];

        //act 4 edit message data


        $message_data = MessageData::find($message->message_data_id);
        $message_data->title = $title;
        $message_data->text = $text;
        $message_data->message_type_id = $message_type_id;
        $message_data->save();


        //act 5 delete old messages and create correct messages

        Message::where('message_data_id', $message_data->id)->delete();

        foreach ($users as $user):

            $new_message = Message::create([
                'from_user_id' => $message->from_user_id,
                'to_user_id' => $user,
                'message_data_id' => $message_data->id,
                'message_status_id' => $message_status,
            ]);

        endforeach;


        return redirect()->route('messages.index', $message->from_user_id)->with('success', $return_message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //todo сделать после механизм удаления записей по изменения статуса записей
        //
    }





    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *
   * Ajax поиски
   *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


    //todo доделать механизм найденных пользователей, выводить имя и почту.
    public function ajax_find_users_to_message(Request $request)
    {
        $input = $request->all();

        $last_name = $input['last_name'];
        $name = $input['name'];
        $email = $input['email'];

        if (!is_null($last_name)) {
            $profiles = Profile::where('last_name', 'like', '%' . $last_name . '%')->get();
            $profile_users_id = $profiles->pluck('user_id')->toArray();

        } else {
            $profiles = null;
            $profile_users_id = null;
        }

        if (is_null($profile_users_id)) {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->get();
        } else {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->whereIn('id', $profile_users_id)->get();
        }

        $users_id = $users->pluck('id')->toArray();

        $result = [];

        foreach ($users as $user) {

            $result[$user->id] = [];
            $result[$user->id]['email'] = $user->email;
            $result[$user->id]['name'] = $user->name;

        }


        return response()->json($result);
    }


}
