<?php

namespace App\Http\Controllers\Users;

use Illuminate\Support\Facades\View;
use App\Models\Users\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        $bom = 'test';
        // Sharing is caring
        View::share('test', $bom);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, $method = 'show')
    {
        if ($method == 'show') {
            $data['title'] = 'Профиль';
        } elseif ($method == 'create') {
            $data['title'] = 'Создание профиля';
        } elseif ($method == 'edit') {
            $data['title'] = 'Изменение профиля';
        }
        $cheked_user = Auth::user();
        $checked_user_id = $cheked_user->id;
        $data['user'] = $user = User::find($id);
        //В зависимости от метода возвращаются разные формы, просмотра, создания или удаления
        $data['method'] = $method;
        if ($id == $checked_user_id || $cheked_user->inRole(1)) {
            // 4 varables true or false = $is_owner $is_admin $is_moder $is_user
            $data = array_merge($data, $user->getRoleVariables());

            $data['profile'] = Profile::where('user_id', $id)->first();

            return view('profiles.index', $data);

        } else {
            return back()->with('error', 'Доступ закрыт');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        $method = 'create';
        $profile = Profile::where('user_id', $id)->first();
        if (is_null($profile)) {
            return redirect()->route('profiles.index', [$id, $method]);
        } else {
            return back()->with('error', 'Данные профиля уже заполнены');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = $request->all();
        $user_id = $request['user_id'];


        if(!isset($request['agree']) || !$request['agree'])
            return back()->withInput()->with('error','Вы не согласились на обработку данных');

        $profile = Profile::where('user_id',$user_id)->first();
        if (is_null($profile)){
            $profile = Profile::create($request);

            $method = 'show';
            return redirect()->route('profiles.index', [$user_id, $method])->with('success','Профиль создан');


        }else{
            return back()->with('error','Профиль уже создан, проверьте исправность мыши, так как возможно вы кликнули 2 раза');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Users\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Users\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $method = 'edit';
        $profile = Profile::where('user_id', $id)->first();
        if (!is_null($profile)) {
            return redirect()->route('profiles.index', [$id, $method]);
        } else {
            return back()->with('error', 'Профиля еще нет, создайте');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\models\Users\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        //todo сделать серверный валидатор на заполнение форм.
        $request = $request->all();

        if(!isset($request['agree']) || !$request['agree'])
            return back()->withInput()->with('error','Вы не согласились на обработку данных');

        $profile = Profile::where('user_id',$id)->first()->update($request);
        $method = 'show';
        return redirect()->route('profiles.index', [$id, $method])->with('success','Профиль изменен');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Users\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
