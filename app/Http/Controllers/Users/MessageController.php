<?php

namespace App\Http\Controllers\Users;

use App\Models\Users\Message;
use App\Models\Users\MessageData;
use App\models\Users\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    //todo будет одна страница с главным блоком сообщений, где можно выбрать отправленные, полученные, черновики и публичные, kind_of отображает по стандарту отправленные = sent
    //work  типы сообщений:  sent -  отправленные, received - полученные, draft - черновики
    public function index($user_id, $kind_of = 'received')
    {
        $data['user'] = $user = User::find($user_id);
        $data['kind_of'] = $kind_of;

        $data['received_new_count'] = Message::where('to_user_id', $user_id)->where('message_status_id', 2)->count();

        //work get received messages
        if ($kind_of == 'received') {

            //act 1 get all messages where not in deleted from receiver or delete
            $messages = Message::where('to_user_id', $user_id)->whereNotIn('message_status_id', [1, 5, 6])->orderBy('id', 'DESC')->get();
            if (is_null($messages) || count($messages) == 0) {
                $data['no_messages'] = 'Нет сообщений';
            } else {
                //act 2 get all related data
                $senders_id = array_unique($messages->pluck('from_user_id')->toArray());
                $senders = User::whereIn('id', $senders_id)->get();
                $messages_data_id = array_unique($messages->pluck('message_data_id')->toArray());
                $messages_data = MessageData::whereIn('id', $messages_data_id)->get();

                //act 3 get supermass

                $supermass = [];

                foreach ($messages as $message):
                    $message_id = $message->id;
                    $supermass[$message_id] = [];
                    $supermass[$message_id]['from'] = $senders->find($message->from_user_id)->name ?? $senders->find($message->from_user_id)->email;
                    $supermass[$message_id]['message_status_id'] = $message->message_status_id;

                    $supermass[$message_id]['title'] = $messages_data->find($message->message_data_id)->title ?? mb_substr($messages_data->find($message->message_data_id)->text, 0, 30) . '...';

                endforeach;

                //todo сделть так, чтбы непрочитанные сообщения всегда были выше тек, которые уже прочитаны


                $data['messages'] = $supermass;
            }
        } //work get sent messages
        elseif ($kind_of == 'sent') {

            //act 1 get all messages where not in deleted from receiver or delete
            $messages = Message::where('from_user_id', $user_id)->whereNotIn('message_status_id', [1, 4, 6])->orderBy('id', 'DESC')->get();
            if (is_null($messages) || count($messages) == 0) {
                $data['no_messages'] = 'Нет сообщений';
            } else {
                //act 2 get all related data
                $receivers_id = array_unique($messages->pluck('to_user_id')->toArray());
                $receivers = User::whereIn('id', $receivers_id)->get();
                $messages_data_id = array_unique($messages->pluck('message_data_id')->toArray());
                $messages_data = MessageData::whereIn('id', $messages_data_id)->get();

                //act 3 get supermass

                $supermass = [];

                foreach ($messages as $message):

                    $message_id = $message->id;
                    $supermass[$message_id] = [];
                    $supermass[$message_id]['from'] = $receivers->find($message->to_user_id)->name ?? $receivers->find($message->to_user_id)->email;
                    $supermass[$message_id]['message_status_id'] = $message->message_status_id;
                    $supermass[$message_id]['title'] = $messages_data->find($message->message_data_id)->title ?? mb_substr($messages_data->find($message->message_data_id)->text, 0, 30) . '...';

                endforeach;
                $data['messages'] = $supermass;
            }

        } //work get draft
        elseif ($kind_of == 'draft') {

            //act 1 get all messages where not in deleted from receiver or delete
            $messages = Message::where('from_user_id', $user_id)->where('message_status_id', 1)->orderBy('id', 'DESC')->get();
            if (is_null($messages) || count($messages) == 0) {
                $data['no_messages'] = 'Нет сообщений';
            } else {
                //act 2 get all related data
                $receivers_id = array_unique($messages->pluck('to_user_id')->toArray());
                $receivers = User::whereIn('id', $receivers_id)->get();
                $messages_data_id = array_unique($messages->pluck('message_data_id')->toArray());
                $messages_data = MessageData::whereIn('id', $messages_data_id)->get();

                //act 3 get supermass
                $supermass = [];
                foreach ($messages as $message):
                    $message_id = $message->id;
                    $supermass[$message_id] = [];
                    $supermass[$message_id]['from'] = $receivers->find($message->to_user_id)->name ?? $receivers->find($message->to_user_id)->email;
                    $supermass[$message_id]['message_status_id'] = $message->message_status_id;
                    $supermass[$message_id]['title'] = $messages_data->find($message->message_data_id)->title ?? mb_substr($messages_data->find($message->message_data_id)->text, 0, 30) . '...';

                endforeach;

                $data['messages'] = $supermass;
            }

        }


        return view('messages.index', $data);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function create()
    {

        $data['user'] = $user = Auth::user();


        return view('messages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    //todo при создании сообщения можно сохранить его как черновик, тогода в получателя пишется отправитель
    //todo при создании сообщений группе людей дублировать сообщения меняя получателя
    public function store(Request $request)
    {
        $input = $request->all();

        //act 1 chek is it a draft
        (isset($input['draft']) && $input['draft']) ? $draft = true : $draft = false;
        //act 1.1 create return message
        ($draft) ? $return_message = 'Сообщение сохранено как черновик' : $return_message = 'Сообщение отправлено';
        //act 1.2 default message status
        ($draft) ? $message_status = 1 : $message_status = 2;
        //act 2 set default message type
        (isset($input['message_type_id'])) ? $message_type_id = $input['message_type_id'] : $message_type_id = 1;

        //act 3 set other varables
        $user_id = $input['user_id'];
        $title = $input['title'];
        $text = $input['text'];
        $users = $input['users'];

        //act 4 create message data

        $message_data = MessageData::create([
            'title' => $title,
            'text' => $text,
            'message_type_id' => $message_type_id,
        ]);

        //act 5 create message

        foreach ($users as $user):

            $message = Message::create([
                'from_user_id' => $user_id,
                'to_user_id' => $user,
                'message_data_id' => $message_data->id,
                'message_status_id' => $message_status,
            ]);

        endforeach;


        return redirect()->route('messages.index', $user_id)->with('success', $return_message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    public function read($message_id, $confirm = false)
    {
        $data['message'] = $message = Message::find($message_id);

        if ($confirm) {
            $message->message_status_id = 3;
            $message->save();
        }

        $data['message_data'] = MessageData::find($message->message_data_id);

        $data['from_user'] = User::find($message->from_user_id);

        $data['to_user'] = User::find($message->to_user_id);


        return view('messages.read', $data);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        $data['message'] = $message;

        $users_id = array_unique(Message::where('message_data_id', $message->message_data_id)->pluck('to_user_id')->toArray());

        $users = User::whereIn('id', $users_id)->get();

        $data['users'] = [];
        $data['selected_users'] = [];

        foreach ($users as $sender):

            $data['users'][$sender->id] = $sender->name . ' ' . $sender->email;
            $data['selected_users'][$sender->id] = $sender->id;

        endforeach;

        $data['message_data'] = MessageData::find($message->message_data_id);

        return view('messages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        $input = $request->all();

        //act 1 chek is it a draft
        (isset($input['draft']) && $input['draft']) ? $draft = true : $draft = false;
        //act 1.1 create return message
        ($draft) ? $return_message = 'Сообщение сохранено как черновик' : $return_message = 'Сообщение отправлено';
        //act 1.2 default message status
        ($draft) ? $message_status = 1 : $message_status = 2;
        //act 2 set default message type
        (isset($input['message_type_id'])) ? $message_type_id = $input['message_type_id'] : $message_type_id = 1;

        //act 3 set other varables

        $title = $input['title'];
        $text = $input['text'];
        $users = $input['users'];

        //act 4 edit message data


        $message_data = MessageData::find($message->message_data_id);
        $message_data->title = $title;
        $message_data->text = $text;
        $message_data->message_type_id = $message_type_id;
        $message_data->save();



        //act 5 delete old messages and create correct messages

        Message::where('message_data_id',$message_data->id)->delete();

        foreach ($users as $user):

            $new_message = Message::create([
                'from_user_id' => $message->from_user_id,
                'to_user_id' => $user,
                'message_data_id' => $message_data->id,
                'message_status_id' => $message_status,
            ]);

        endforeach;


        return redirect()->route('messages.index', $message->from_user_id)->with('success', $return_message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //todo сделать после механизм удаления записей по изменения статуса записей
        //
    }





    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *
   * Ajax поиски
   *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


    //todo доделать механизм найденных пользователей, выводить имя и почту.
    public function ajax_find_users_to_message(Request $request)
    {
        $input = $request->all();

        $last_name = $input['last_name'];
        $name = $input['name'];
        $email = $input['email'];

        if (!is_null($last_name)) {
            $profiles = Profile::where('last_name', 'like', '%' . $last_name . '%')->get();
            $profile_users_id = $profiles->pluck('user_id')->toArray();

        } else {
            $profiles = null;
            $profile_users_id = null;
        }

        if (is_null($profile_users_id)) {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->get();
        } else {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->whereIn('id', $profile_users_id)->get();
        }

        $users_id = $users->pluck('id')->toArray();

        $result = [];

        foreach ($users as $user) {

            $result[$user->id] = [];
            $result[$user->id]['email'] = $user->email;
            $result[$user->id]['name'] = $user->name;

        }


        return response()->json($result);
    }


}
