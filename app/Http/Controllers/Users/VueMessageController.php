<?php

namespace App\Http\Controllers\Users;

use App\Http\Traits\MessageTrait;
use App\Models\Users\Message;
use App\Models\Users\MessageData;
use App\Models\Users\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class VueMessageController extends Controller
{
    use MessageTrait;

    /*******************************************************
     * Как всё это работает?
     * Здесь мы обращаемся к трейту и получаем из него данные в виде массива, затем преобразуем в json с необходимым статусом
     *
     ************/


    /**
     * Получение всех писем определенного типа, где kind_of: sent -  отправленные, received - полученные, draft - черновики
     * Входящие параметры: id пользователя системы, тип сообщений
     * Исходящие параметры: json
     */

    public function index($user_id, $kind_of = 'received')
    {
        $data = $this->getAllMessages($user_id,$kind_of);
        return response()->json($data, 200);
    }

    public function read($message_id)
    {
        $data = $this->readMessage($message_id);
        return response()->json($data, 200);
    }





















    public function create()
    {

        $data['user'] = $user = Auth::user();


        return view('messages.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $input = $request->all();

        //act 1 chek is it a draft
        (isset($input['draft']) && $input['draft']) ? $draft = true : $draft = false;
        //act 1.1 create return message
        ($draft) ? $return_message = 'Сообщение сохранено как черновик' : $return_message = 'Сообщение отправлено';
        //act 1.2 default message status
        ($draft) ? $message_status = 1 : $message_status = 2;
        //act 2 set default message type
        (isset($input['message_type_id'])) ? $message_type_id = $input['message_type_id'] : $message_type_id = 1;

        //act 3 set other varables
        $user_id = $input['user_id'];
        $title = $input['title'];
        $text = $input['text'];
        $users = $input['users'];

        //act 4 create message data

        $message_data = MessageData::create([
            'title' => $title,
            'text' => $text,
            'message_type_id' => $message_type_id,
        ]);

        //act 5 create message

        foreach ($users as $user):

            $message = Message::create([
                'from_user_id' => $user_id,
                'to_user_id' => $user,
                'message_data_id' => $message_data->id,
                'message_status_id' => $message_status,
            ]);

        endforeach;


        return redirect()->route('messages.index', $user_id)->with('success', $return_message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        $data['message'] = $message;

        $users_id = array_unique(Message::where('message_data_id', $message->message_data_id)->pluck('to_user_id')->toArray());

        $users = User::whereIn('id', $users_id)->get();

        $data['users'] = [];
        $data['selected_users'] = [];

        foreach ($users as $sender):

            $data['users'][$sender->id] = $sender->name . ' ' . $sender->email;
            $data['selected_users'][$sender->id] = $sender->id;

        endforeach;

        $data['message_data'] = MessageData::find($message->message_data_id);

        return view('messages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        $input = $request->all();

        //act 1 chek is it a draft
        (isset($input['draft']) && $input['draft']) ? $draft = true : $draft = false;
        //act 1.1 create return message
        ($draft) ? $return_message = 'Сообщение сохранено как черновик' : $return_message = 'Сообщение отправлено';
        //act 1.2 default message status
        ($draft) ? $message_status = 1 : $message_status = 2;
        //act 2 set default message type
        (isset($input['message_type_id'])) ? $message_type_id = $input['message_type_id'] : $message_type_id = 1;

        //act 3 set other varables

        $title = $input['title'];
        $text = $input['text'];
        $users = $input['users'];

        //act 4 edit message data


        $message_data = MessageData::find($message->message_data_id);
        $message_data->title = $title;
        $message_data->text = $text;
        $message_data->message_type_id = $message_type_id;
        $message_data->save();



        //act 5 delete old messages and create correct messages

        Message::where('message_data_id',$message_data->id)->delete();

        foreach ($users as $user):

            $new_message = Message::create([
                'from_user_id' => $message->from_user_id,
                'to_user_id' => $user,
                'message_data_id' => $message_data->id,
                'message_status_id' => $message_status,
            ]);

        endforeach;


        return redirect()->route('messages.index', $message->from_user_id)->with('success', $return_message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Users\Message $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //todo сделать после механизм удаления записей по изменения статуса записей
        //
    }





    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
   *
   * Ajax поиски
   *
   * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


    //todo доделать механизм найденных пользователей, выводить имя и почту.
    public function ajax_find_users_to_message(Request $request)
    {
        $input = $request->all();

        $last_name = $input['last_name'];
        $name = $input['name'];
        $email = $input['email'];

        if (!is_null($last_name)) {
            $profiles = Profile::where('last_name', 'like', '%' . $last_name . '%')->get();
            $profile_users_id = $profiles->pluck('user_id')->toArray();

        } else {
            $profiles = null;
            $profile_users_id = null;
        }

        if (is_null($profile_users_id)) {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->get();
        } else {
            $users = User::where('name', 'like', '%' . $name . '%')->where('email', 'like', '%' . $email . '%')->whereIn('id', $profile_users_id)->get();
        }

        $users_id = $users->pluck('id')->toArray();

        $result = [];

        foreach ($users as $user) {

            $result[$user->id] = [];
            $result[$user->id]['email'] = $user->email;
            $result[$user->id]['name'] = $user->name;

        }


        return response()->json($result);
    }


}
