<?php

namespace App\Http\Middleware;



use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next, ... $roles)
    {

        if (!Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
            return redirect()->route('login')->with('error','Вы не авторизованы');

        $user = Auth::user();

        //Владельцу можно переходить куда угодно не зависимо от ограничений
        if($user->inRole('owner'))
            return $next($request);



        foreach($roles as $role) {
            // Check if user has the role This check will depend on how your roles are set up
            if($user->inRole($role))
                return $next($request);
        }

        return redirect()->route('home')->with('error','Нет доступа');
    }
}
