<?php

namespace App\Http\Traits;

use App\Models\Users\Message;
use App\Models\Users\MessageData;
use App\User;

trait MessageTrait
{


    /*******************************************************
     * Как всё это работает?
     * VUE связан с laravel и использует роли laravel для работы со своими методами
     * Api использует passport laravel для авторизации и получения доступа к работе с данным контроллером.
     * Трейт сделан для приватных методов
     * Работа на уровне Vue идет в js/components/MessagesComponent.vue
     * Работа и тестирование для Api производится через браузер с помощью расширения Restlet Client
     *
     * Как работать в Restlet Client?
     * http://taskziz/api/login  логинимся используя форму POST
     * {"email":"votoziz@gmail.com","password":"100500Qq","text":"tested"}
     * копируем токен ответ, огромную строку
     * http://taskziz/api/auth/messages/index/user_id=1/kind_of=received это тестирование через GET запрос, где
     * Нужно указать headers
     * Accept -> application/json
     * Authorization ->  Bearer (ПРОБЕЛ) тут скопированный огромный токен
     * Всё, получаем ответ
     *
     ************/




    /**
     * Получение данных links:{first,last,prev} , meta:{current_page,from,last_page,path,per_page,to,total}
     * Входящие параметры: object messages
     * Исходящие параметры: some $data
     * исходящую дату нужно смерджить с существующей
     * используется в  getAllMessages()
     */

    private function get_messages_pagination($messages)
    {
        $message_json = json_encode($messages);
        $message_json = json_decode($message_json);
        $data['links'] = [];
        $data['links']['first'] = $message_json->first_page_url;
        $data['links']['last'] = $message_json->last_page_url;
        $data['links']['next'] = $message_json->next_page_url;
        $data['links']['prev'] = $message_json->prev_page_url;
        $data['meta'] = [];
        $data['meta']['current_page'] = $message_json->current_page ?? 1;
        $data['meta']['from'] = $message_json->from;
        $data['meta']['last_page'] = $message_json->last_page;
        $data['meta']['path'] = $message_json->path;
        $data['meta']['per_page'] = $message_json->per_page;
        $data['meta']['to'] = $message_json->to;
        $data['meta']['total'] = $message_json->total;
        return $data;
    }







///////////////////////////////////////////////////////пока не рабочие методы/////////////////////////////////////////////////////////////


    public function storeEntry($json_response)
    {

        $status = 201;
        $danger = false;
        $response = 'success';

        //рестлер клиент передает json как объект
        if (is_object($json_response)) {
            $json_response = $json_response->getContent();
        }
        $input = json_decode($json_response);

        if (!isset($input->username) || !isset($input->email) || !isset($input->text)) {
            $response = 'invalid fields (should be: username, email, text)';
            $danger = true;
            $status = 400;

        } elseif ($input->username === '' || $input->email === '' || $input->text === '') {
            $response = 'empty fields';
            $danger = true;
            $status = 400;
        }

        if ($status == 201) {
            //todo сделать обработку возможных ошибок
            $new_entry = Entry::create([
                'username' => $input->username,
                'email' => $input->email,
                'text' => $input->text,
            ]);
        }

        $data['danger'] = $danger;
        $data['response'] = $response;
        $data['status'] = $status;

        return $data;
    }

    public function updateEntry($json_response)
    {
        $status = 201;
        $danger = false;
        $response = 'success';

        if (is_object($json_response)) {
            $json_response = $json_response->getContent();
        }
        $input = json_decode($json_response);

        if (!isset($input->id) || !isset($input->text)) {
            $response = 'invalid fields (should be: id, text)';
            $danger = true;
            $status = 400;

        } elseif ($input->id === '' || $input->text === '') {
            $response = 'empty fields';
            $danger = true;
            $status = 400;
        }

        if ($status == 201) {

            //todo сделать обработку возможных ошибок
            $entry = Entry::find($input->id);
            $entry->text = $input->text;
            $entry->save();

        }

        $data['danger'] = $danger;
        $data['response'] = $response;
        $data['status'] = $status;

        return $data;
    }

    public function deleteEntry($json_response)
    {
        $status = 200;
        $danger = false;
        $response = 'success';

        if (is_object($json_response)) {
            $json_response = $json_response->getContent();
        }
        $input = json_decode($json_response);

        if (!isset($input->id)) {
            $response = 'invalid fields (should be: id)';
            $danger = true;
            $status = 400;

        } elseif ($input->id === '') {
            $response = 'empty fields';
            $danger = true;
            $status = 400;
        }

        if ($status == 200) {
            $entry = Entry::find($input->id);;
            if (is_null($entry)) {
                $response = 'Entry not found or already deleted';
                $danger = true;
                $status = 404;
            } else {
                $entry->delete();
            }
        }

        $data['danger'] = $danger;
        $data['response'] = $response;
        $data['status'] = $status;

        return $data;
    }


}