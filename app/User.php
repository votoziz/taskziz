<?php

namespace App;

use App\Models\Users\RoleInterface;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static $rolesModel = 'App\Models\Users\Role';

    //Получить роли
    public function role()
    {
        return $this->belongsTo(static::$rolesModel);
    }

    public function getRole()
    {
        return $this->role;
    }

    //Function return 4 variables null or 1;
    public function getRoleVariables()
    {
        $data['is_owner'] = false;
        $data['is_admin'] = false;
        $data['is_moder'] = false;
        $data['is_user'] = false;
        $role = $this->getRole();

        if (!is_null($role)) {

            if ($role->name === 'owner') {
                $data['is_owner'] = true;
                $data['is_admin'] = true;
                $data['is_moder'] = true;
                $data['is_user'] = true;
            } elseif ($role->name === 'admin') {
                $data['is_admin'] = true;
                $data['is_moder'] = true;
                $data['is_user'] = true;
            } elseif ($role->name === 'moder') {
                $data['is_moder'] = true;
                $data['is_user'] = true;
            } elseif ($role->name === 'user') {
                $data['is_user'] = true;
            }
        }

        return $data;
    }


    public function inRole($role)
    {

        //Система ролей, сделана таким образом, что у пользователя всего одна роль, но более высокая роль имеет возможность использовать механизмы для более низких ролей.
        if ($role instanceof RoleInterface) {
            $roleId = $role->getRoleId();
        }

        $instance = $this->role;

        if (!is_null($instance)) {


            if ($role instanceof RoleInterface) {
                if ($instance->getRoleId() <= $roleId) {
                    return true;
                }
            } else {
                if ($instance->getRoleId() <= $role || $instance->getRoleSlug() == $role) {

                    return true;

                } elseif ($instance->getRoleSlug() == 'admin') {
                    if ($role == 'moder') {
                        return true;
                    } elseif ($role == 'user') {
                        return true;
                    }
                } elseif ($instance->getRoleSlug() == 'moder') {
                    if ($role == 'user') {
                        return true;
                    }
                }
            }
        }


        return false;
    }


}
