<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AssetServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        $this->publishes([
            base_path('vendor/twbs/bootstrap/dist/css/bootstrap.min.css') => public_path('vendor/bootstrap/css/bootstrap.min.css'),
            base_path('vendor/twbs/bootstrap/dist/css/bootstrap-grid.min.css') => public_path('vendor/bootstrap/css/bootstrap-grid.min.css'),
            base_path('vendor/twbs/bootstrap/dist/css/bootstrap-reboot.min.css') => public_path('vendor/bootstrap/css/bootstrap-reboot.min.css'),

            base_path('vendor/twbs/bootstrap/dist/js/bootstrap.min.js') => public_path('vendor/bootstrap/js/bootstrap.min.js'),
            base_path('vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js') => public_path('vendor/bootstrap/js/bootstrap.bundle.min.js'),
        ], 'bootstrap');
        //добавление на обновление jquery загруженного в модуле components для добавления новых обновлений https://www.davidvandertuijn.nl/laravel-update-bootstrap-with-composer
        $this->publishes([
            base_path('vendor/components/jquery/jquery.min.js') => public_path('vendor/jquery/js/jquery.min.js'),
            ], 'jquery');
//
//        $this->publishes(
//            [
//                base_path('vendor/ckeditor/ckeditor/ckeditor.js') => public_path('vendor/ckeditor/ckeditor/ckeditor.js'),
//                base_path('vendor/ckeditor/ckeditor/config.js') => public_path('vendor/ckeditor/ckeditor/config.js'),
//                base_path('vendor/ckeditor/ckeditor/styles.js') => public_path('vendor/ckeditor/ckeditor/styles.js'),
//                base_path('vendor/ckeditor/ckeditor/contents.css') => public_path('vendor/ckeditor/ckeditor/contents.css'),
//                base_path('vendor/ckeditor/ckeditor/adapters') => public_path('vendor/ckeditor/ckeditor/adapters'),
//                base_path('vendor/ckeditor/ckeditor/lang') => public_path('vendor/ckeditor/ckeditor/lang'),
//                base_path('vendor/ckeditor/ckeditor/skins') => public_path('vendor/ckeditor/ckeditor/skins'),
//                base_path('vendor/ckeditor/ckeditor/plugins') => public_path('vendor/ckeditor/ckeditor/plugins'),
//            ],
//            'cke'
//        );

        $this->publishes(
            [
                base_path('vendor/select2/select2/dist/css/select2.min.css') => public_path('vendor/select2/css/select2.min.css'),
                base_path('vendor/select2/select2/dist/js/select2.min.js') => public_path('vendor/select2/js/select2.min.js'),
            ],
            'select2'
        );
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}