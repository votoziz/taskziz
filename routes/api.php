<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('register', 'RegisterController');
        Route::post('login', 'LoginController');
        Route::post('logout', 'LogoutController')->middleware('auth:api');
    });
});


Route::group(['prefix' => 'auth', 'middleware' => 'auth:api'], function () {

    Route::get('/messages/index/user_id={user_id}/kind_of={kind_of?}', ['as' => 'api.messages.index', 'uses' => 'Api\ApiMessageController@index']);
    Route::get('/messages/read/message_id={message_id}/{first_time_read?}', ['as' => 'api.messages.read', 'uses' => 'Api\ApiMessageController@read']);
    Route::post('/messages/search_users', ['as' => 'api.messages.search_users', 'uses' => 'Api\ApiMessageController@search_users']);
    Route::post('/messages/store', ['as' => 'api.messages.store', 'uses' => 'Api\ApiMessageController@store']);


});