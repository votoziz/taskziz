<?php
Route::post('formSubmit','PostController@formSubmit');



Auth::routes(['verify' => true]);

Route::get('/', function () {
    return redirect()->route('home');
});

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

//Группа для авторизованных пользователей
Route::group(['prefix' => 'auth', 'middleware' => ['auth']], function () {



    Route::get('/testpage', ['as' => 'testpage', 'uses' => 'HomeController@testpage']);

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     * users\ProfileController   Контроллер профилей
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    Route::resource('profiles', 'users\ProfileController');
    Route::get('/profiles/index/{id}/{method?}', ['as' => 'profiles.index', 'uses' => 'users\ProfileController@index']);
    Route::get('/profiles/create/{id}', ['as' => 'profiles.create', 'uses' => 'users\ProfileController@create']);




});

//Группа для пользователей, которые подтвердили свою почту
Route::group(['prefix' => 'verified', 'middleware' => ['verified']], function () {


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    *
    * users\MessageController   Контроллер сообщений ? Ajax поиски
    *
    * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    Route::get('/messages/ajax_find_users_to_message', ['as' => 'ajax_find_users_to_message', 'uses' => 'users\MessageController@ajax_find_users_to_message']);



    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     * users\MessageController   Контроллер сообщений
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    Route::resource('messages', 'users\MessageController');
    Route::get('/messages/index/{user_id}/{kind_of?}', ['as' => 'messages.index', 'uses' => 'users\MessageController@index']);
    Route::get('/messages/read/{message_id}/{confirm?}', ['as' => 'messages.read', 'uses' => 'users\MessageController@read']);


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
       *
       * Api\ApiMessageController   Контроллер сообщений для vue.js возвращает json todo это рабочий был VueMessageController
       *
       * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    Route::get('vue/messages/index/{user_id}/{kind_of?}', ['as' => 'vue.messages.index', 'uses' => 'Api\ApiMessageController@index']);
    Route::get('vue/messages/read/{message_id}/{first_time_read?}', ['as' => 'vue.messages.read', 'uses' => 'Api\ApiMessageController@read']);

    Route::post('vue/messages/search_users', ['as' => 'vue.messages.search_users', 'uses' => 'Api\ApiMessageController@search_users']);
    Route::post('vue/messages/store', ['as' => 'vue.messages.store', 'uses' => 'Api\ApiMessageController@store']);

});


//Группа для владельца
Route::group(['prefix' => 'owner', 'middleware' => ['role:owner']], function () {
    Route::get('/testpage', ['as' => 'testpage2', 'uses' => 'HomeController@testpage']);

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     * Owner\OwnerController   Контроллер владельца
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    Route::get('/all_users', ['as' => 'all_users', 'uses' => 'Owner\OwnerController@all_users']);

    Route::get('/change_role/{user_id}/{role_id}/{current_page}', ['as' => 'change_role', 'uses' => 'Owner\OwnerController@change_role']);
});






//Группа для админов
Route::group(['prefix' => 'admin', 'middleware' => ['role:admin']], function () {
    Route::get('/testpage', ['as' => 'testpage3', 'uses' => 'HomeController@testpage']);
});

//Группа для модераторов
Route::group(['prefix' => 'moder', 'middleware' => ['role:moder']], function () {
    Route::get('/testpage', ['as' => 'testpage4', 'uses' => 'HomeController@testpage']);
});

//Группа для юзеров
Route::group(['prefix' => 'user', 'middleware' => ['role:user']], function () {
    Route::get('/testpage', ['as' => 'testpage5', 'uses' => 'HomeController@testpage']);
});

