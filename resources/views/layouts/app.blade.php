<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

{{--Это стандартный механизм перевода, если перевода нет, выводится так как есть, файл перевода resourses/lang/ru.json--}}
{{--{{ __('E-Mail Address') }}--}}


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>


    {{--BOOTSTRAP 4--}}
    {{ Html::style('vendor/bootstrap/css/bootstrap.min.css') }}

    {{--JQUERY--}}
    {{ Html::script('vendor/jquery/js/jquery.min.js') }}

    {{--BOOTSTRAP--}}
    {{ Html::script('vendor/bootstrap/js/bootstrap.min.js') }}

    {{--FONT AWESOME--}}
    {{ Html::style('vendor/fontawesome/css/all.min.css') }}
    {{ Html::script('vendor/fontawesome/js/all.min.js') }}

    {{--SELECT 2--}}
    {{ Html::style('vendor/select2/css/select2.min.css') }}
    {{ Html::script('vendor/select2/js/select2.min.js') }}


    {{--MY ADDITIONS BOOTSTRAP 4--}}
    {{ Html::style('css/mybootstrap4.css') }}

</head>
<body style="background: @yield('background','#343a40') ">
<div id="app">
    <nav class="navbar navbar-expand-md navbar-dark navbar-laravel">
        <div class="container ">
            <a class="navbar-brand" href="{{ route('home') }}">
                {{ config('app.name', 'Taskziz') }}
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">

                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Вход') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Регистрация') }}</a>
                            </li>
                        @endif
                        @else
                            <li class="nav-item dropdown">

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Выход') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>


                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        <div class="col-12">
            <div class="row justify-content-center">
                <div class="col-7">
                    @include('notifications')
                </div>
            </div>
        </div>
        @yield('content')
    </main>
</div>

{{--{{ Html::script('vendor/bootstrap/js/bootstrap.min.js') }}--}}
{{--Мои автоматически вызываемые скрипты--}}
{{ Html::script('js/autoscripts.js') }}

{{--vue--}}
{{ Html::script('js/app.js') }}


</body>
</html>
