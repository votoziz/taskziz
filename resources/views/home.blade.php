@extends('layouts.app')
@section('title', 'Главная')
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if($is_owner)
                @include('Owner.includes.panel')
            @endif

            <div class="col-12">
                <div class="card bg-dim ">
                    <div class="card-header text-center font-weight-bold text-white">Главная</div>

                    <div class="card-body bg-light">


                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-12 pb-4">
                                <a class="btn  bg-dim btn-block" href="{{route('profiles.index',$user->id)}}">
                                    <i class="fas fa-user-tie"></i>
                                    Мой кабинет</a>
                            </div>

                            <div class="col-lg-4 col-md-6 col-12  pb-4">
                                <a class="btn  bg-dim btn-block" href="{{route('messages.index',$user->id)}}">
                                    <i class="fas fa-envelope"></i>
                                    Мои сообщения
                                </a>
                            </div>

                            <div class="col-lg-4 col-md-6 col-12  pb-4">
                                {{--<a class="btn  bg-dim btn-block" href="{{route('vue.messages',$user->id)}}">--}}
                                    {{--<i class="fas fa-envelope"></i>--}}
                                    {{--Мои сообщения Vue+api--}}
                                {{--</a>--}}
                            </div>
                        </div>


                        @if($is_owner)
                            Я владелец
                            {{$test}}
                            <br>
                        @endif
                        @if($is_admin)
                            Я админ
                            {{$test}}
                            <br>
                        @endif
                        @if($is_moder)
                            Я модер
                            {{$test}}
                            <br>
                        @endif
                        @if($is_user)
                            Я юзер
                            {{$test}}
                            <br>
                        @endif


                        <a href="{{route('testpage')}}">Перейти на тестовую</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
