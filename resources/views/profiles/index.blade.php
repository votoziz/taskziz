@extends('layouts.app')
@section('title', $title ?? 'Мой профиль')
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @if($is_owner)
                @include('Owner.includes.panel')
            @endif

            <div class="col-12">
                <div class="card bg-dim tex ">
                    <div class="card-header text-center font-weight-bold text-white">Личный кабинет
                        пользователя {{$user->email}}
                    </div>

                    <div class="card-body bg-light pl-5 pr-5">

                        <div>

                            <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
                                <div class="col-lg-4 col-12 font-weight-bold ">
                                    Отображаемое имя:
                                </div>
                                <div class="col-lg-8 col-12 ">
                                    {{$user->name}}
                                </div>
                            </div>

                            <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
                                <div class="col-lg-4 col-12 font-weight-bold ">
                                    Почта:
                                </div>
                                <div class="col-lg-8 col-12 ">
                                    {{$user->email}}
                                </div>
                            </div>

                            <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
                                <div class="col-lg-4 col-12 font-weight-bold ">
                                    Роль:
                                </div>
                                <div class="col-lg-8 col-12 ">

                                    @if($is_owner)
                                        Владелец
                                    @elseif($is_admin)
                                        Админ
                                    @elseif($is_moder)
                                        Модер
                                    @elseif($is_user)
                                        Пользователь
                                    @else
                                        Гость
                                    @endif
                                </div>
                            </div>

                            <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
                                <div class="col-lg-4 col-12 font-weight-bold ">
                                    Почта подтверждена:
                                </div>
                                <div class="col-lg-8 col-12 ">

                                    @if(!is_null($user->email_verified_at))
                                        {{$user->email_verified_at}}
                                    @else
                                        Не подтверждена
                                    @endif

                                </div>
                            </div>

                            <hr>

                            <div class="row  br-3  mt-1 mb-1">
                                <div class="col-12">
                                    <h2>Данные профиля:</h2>
                                </div>
                            </div>
                            <hr>


                            @if($method == 'show')
                                @include('profiles.includes.show')
                            @elseif($method == 'create')
                                @include('profiles.includes.create')
                            @elseif($method == 'edit')
                                @include('profiles.includes.edit')
                            @endif


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
