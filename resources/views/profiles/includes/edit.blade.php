{!! Form::open(['route' => ['profiles.update',$user->id], 'class' => 'needs-validation',  'novalidate','method' => 'PUT']) !!}

<div class="form-group">
    {!! Form::label('first_name', 'Имя:') !!}
    {{--Образец для валидации--}}
    {{--{!! Form::text('first_name', null, ['class' => 'form-control form-control-sm','required']) !!}--}}
    {{--<div class="invalid-feedback">Обязательное для заполнения</div>--}}

    {!! Form::text('first_name', $profile->first_name, ['class' => 'form-control form-control-sm']) !!}

</div>


<div class="form-group">
    {!! Form::label('last_name', 'Фамилия:') !!}
    {!! Form::text('last_name', $profile->last_name, ['class' => 'form-control form-control-sm']) !!}

</div>

<div class="form-group">
    {!! Form::label('second_name', 'Отчество:') !!}
    {!! Form::text('second_name', $profile->second_name, ['class' => 'form-control form-control-sm']) !!}

</div>

<div class="form-group">
    {!! Form::label('phone', 'Телефон:') !!}
    {!! Form::text('phone', $profile->phone, ['class' => 'form-control form-control-sm']) !!}
</div>


<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" value="true" name="agree"
               id="agree">
        <label class="custom-control-label" for="agree"> Согласен на обработку персональных данных</label>
    </div>
</div>

<br>
{!! Form::submit( 'Редактировать',  ["class" =>  "btn btn-success btn-block"]) !!}
{!! Form::close() !!}
