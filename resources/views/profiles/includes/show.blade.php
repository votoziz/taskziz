@if(is_null($profile))
    <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
        <div class="col-12 font-weight-bold ">
            Данные профиля еще не заполнены
        </div>
    </div>
    <div class="row bg-light-dim br-3 mt-1 mb-1">
        <a class="btn btn-success btn-block"
           href="{{route('profiles.create',$user->id)}}">Заполнить данные профиля</a>
    </div>
@else
    <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
        <div class="col-12 col-lg-4 font-weight-bold ">
            Имя:
        </div>
        <div class="col-12 col-lg-8  ">
            {{$profile->first_name}}
        </div>
    </div>

    <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
        <div class="col-12 col-lg-4 font-weight-bold ">
            Фамилия:
        </div>
        <div class="col-12 col-lg-8  ">
            {{$profile->last_name}}
        </div>
    </div>

    <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
        <div class="col-12 col-lg-4 font-weight-bold ">
            Отчество:
        </div>
        <div class="col-12 col-lg-8  ">
            {{$profile->second_name}}
        </div>
    </div>

    <div class="row bg-light-dim br-3 hoverer mt-1 mb-1">
        <div class="col-12 col-lg-4 font-weight-bold ">
            Телефон:
        </div>
        <div class="col-12 col-lg-8  ">
            {{$profile->phone}}
        </div>
    </div>

    <br>
    <div class="row bg-light-dim br-3 mt-1 mb-1">
        <a class="btn btn-info btn-block"
           href="{{route('profiles.edit',$user->id)}}">Редактировать данные профиля</a>
    </div>

@endif