{!! Form::open(['route' => ['profiles.store'], 'class' => 'needs-validation',  'novalidate']) !!}
{!! Form::hidden("user_id",$user->id) !!}

<div class="form-group">
    {!! Form::label('first_name', 'Имя:') !!}
    {{--Образец для валидации--}}
    {{--{!! Form::text('first_name', null, ['class' => 'form-control form-control-sm','required']) !!}--}}
    {{--<div class="invalid-feedback">Обязательное для заполнения</div>--}}

    {!! Form::text('first_name', null, ['class' => 'form-control form-control-sm']) !!}

</div>


<div class="form-group">
    {!! Form::label('last_name', 'Фамилия:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control form-control-sm']) !!}

</div>

<div class="form-group">
    {!! Form::label('second_name', 'Отчество:') !!}
    {!! Form::text('second_name', null, ['class' => 'form-control form-control-sm']) !!}

</div>

<div class="form-group">
    {!! Form::label('phone', 'Телефон:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control form-control-sm']) !!}
</div>



<div class="form-group">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" value="true" name="agree"
               id="agree">
        <label class="custom-control-label" for="agree"> Согласен на обработку персональных данных</label>
    </div>
</div>
<br>

{!! Form::submit( 'Создать',  ["class" =>  "btn btn-success btn-block"]) !!}
{!! Form::close() !!}
