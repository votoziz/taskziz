@extends('layouts.app')
@section('title', $title ?? 'Мои сообщения')
@section('content')

    @include('messages.includes.add_reciver_modal')

    <div class="container">
        <div class="row justify-content-center">



            <div class="col-12">
                <div class="card bg-dim tex ">
                    <div class="card-header text-center font-weight-bold text-white">Редактирование сообщения
                    </div>




                    <div class="card-body bg-light pl-5 pr-5">

                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#add_reciver">
                            Добавить получателя
                        </button>

                        <button type="button" class="btn btn-danger" id="clear_reciver">
                          Очистить
                        </button>

                        <hr>

                        {!! Form::open(["route" => ['messages.update',$message->id] ,'method' => 'PUT',  'class' => 'needs-validation',  'novalidate'] ) !!}



                        <div class="form-group">
                            {!! Form::label('users[]', 'Соавторы:') !!}
                            {!! Form::select('users[]', $users, $selected_users,["class" => "form-control form-control-sm no_events", "id"=>"selected_users", 'multiple'=>'multiple','required']) !!}
                            <div class="invalid-feedback">Нужено хотя бы один получатель</div>
                        </div>


                        <div class="form-group">

                            {!! Form::label('title', 'Тема:') !!}
                            {!! Form::text("title",$message_data->titile, ["class" => "form-control", "placeholder" => ""]) !!}

                        </div>
                        <div class="form-group">
                            {!! Form::label('text', 'Сообщение:') !!}
                            {!! Form::textarea("text", $message_data->text,["class" => "form-control", "placeholder" => "",'required']) !!}
                            <div class="invalid-feedback">напишите что-нибудь</div>

                        </div>




                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="true" name="draft"
                                       id="draft">
                                <label class="custom-control-label" for="draft"> Сохранить как черновик </label>
                            </div>
                        </div>





                        {!! Form::submit( "Отправить" , ["class" => "btn btn-outline-dark btn-lg btn-block"]) !!}
                        {{ Form::close() }}


                        <hr>

                    </div>

                </div>
            </div>
        </div>

    </div>

    <script>
        $('#clear_reciver').on('click',function () {
            $('#selected_users').empty();
        })

    </script>

@endsection
