@extends('layouts.app')
@section('title', $title ?? 'Сообщение')
@section('content')

    @include('messages.includes.add_reciver_modal')

    <div class="container">
        <div class="row justify-content-center">


            <div class="col-12">
                <div class="card bg-dim tex ">
                    <div class="card-header text-center font-weight-bold text-white">{{$message_data->title ?? 'Нет темы'}}
                    </div>
                    <div class="card card-body">
                        {{$message_data->text}}
                    </div>

                    <div class="card card-footer">
                        ОТПРАВИТЕЛЬ: {{$from_user->name ?? $from_user->email}}
                        ПОЛУЧАТЕЛЬ: {{$to_user->name ?? $to_user->email}} Дата создания: {{$message_data->created_at}}
                    </div>


                    {{--Если это черновик--}}
                    @if($message->message_status_id == 1)
                        <a href="{{route('messages.edit',$message->id)}}"  class=" btn btn-success">Редактировать и отправить</a>

                    @endif


                </div>
            </div>
        </div>

    </div>


@endsection
