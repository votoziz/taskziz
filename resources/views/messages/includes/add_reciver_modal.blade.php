<!-- Modal -->
<div class="modal fade" id="add_reciver" tabindex="-1" role="dialog" aria-labelledby="add_reciverLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="add_reciverLabel">Найти получателя</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group col-12 ">
                    <h2><span class="badge badge-secondary">Имя</span></h2>
                    {!! Form::text('name', null, ['class' => 'form-control ajax_finder', 'id'=>'name']) !!}

                    <h2><span class="badge badge-secondary">Почта</span></h2>
                    {!! Form::text('email', null, ['class' => 'form-control ajax_finder', 'id'=>'email']) !!}

                    <h2><span class="badge badge-secondary">Фамилия</span></h2>
                    {!! Form::text('last_name', null, ['class' => 'form-control ajax_finder', 'id'=>'last_name']) !!}

                    <br>
                    <div class="btn btn-success">Поиск</div>


                </div>

                <div class="col-12 ajax_resulter">



                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                <button type="button" class="btn btn-success">Подтвердить выбор</button>
            </div>
        </div>
    </div>
</div>


<script>



    $('.ajax_finder').change(function (event) {
        $.ajax(
            {   async: true,
//                minlenght:3,
                data: {
                    name:  $('#name').val(),
                    email:  $('#email').val(),
                    last_name:  $('#last_name').val()
                },

                dataType: "html",
                success: function (data) {
                  console.log(data);


                    let users = JSON.parse(data);
                    $('.ajax_resulter').empty();


                    for (var user_id in users) {
                        let user_mail =  users[user_id].email;
                        let user_name =  users[user_id].name;
                        $('.ajax_resulter').append('<div class="btn btn-info btn-block user_selector" id="'+user_id+'" onclick="user_selector_f('+user_id+')">'+user_name+' '+user_mail +'</div>  ');
                    }
                    //todo здесь код с выводом результатов и занесением их в input. удаление старых поисков, перезаписк их новыми каждый раз



                },
                type: "GET",
                url: "{{route('ajax_find_users_to_message')}}"
            }
        );





        return false;
    });



        function user_selector_f(user_id,user_mail) {
            $('#add_reciver').modal('hide');

            var this_id = user_id;

            var this_html = $('#'+this_id).html();

            $('#selected_users').append('<option value="'+this_id+'" selected="selected">'+this_html+'</option>');



            console.log(this_id,this_html );
        }





</script>


