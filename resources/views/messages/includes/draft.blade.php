Черновики:
<hr>
<br>
@if(isset($no_messages))
    {{$no_messages}}
@else
    @foreach($messages as $message_id => $message)
        <a href="{{route('messages.read',$message_id)}}" class="btn btn-block btn-lg btn-outline-secondary text-left">
            КОМУ: {{$message['from']}} ТЕМА: {{$message['title']}}
        </a>
    @endforeach
@endif