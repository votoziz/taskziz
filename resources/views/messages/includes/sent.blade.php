Исходящие сообщения:
<hr>
<br>
@if(isset($no_messages))
    {{$no_messages}}
@else
    @foreach($messages as $message_id => $message)
        <a href="{{route('messages.read',$message_id)}}" class="btn btn-block btn-lg btn-outline-secondary text-left">
            @if($message['message_status_id'] == 3)
                <i class="fas fa-check-square"></i>
                @else
                <i class="fas fa-window-close"></i>
            @endif
            КОМУ: {{$message['from']}} ТЕМА: {{$message['title']}}
        </a>
    @endforeach
@endif