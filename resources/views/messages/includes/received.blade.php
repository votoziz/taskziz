Входящие сообщения:
<hr>
<br>
@if(isset($no_messages))
    {{$no_messages}}
@else
    @foreach($messages as $message_id => $message)
        <a href="{{route('messages.read',[$message_id, $confirm = true])}}" class="btn btn-block btn-lg btn-outline-secondary text-left">
            @if($message['message_status_id'] == 2)
                <i class="fa fa-plus-square"></i>
            @endif
            ОТ: {{$message['from']}} ТЕМА: {{$message['title']}}
        </a>
    @endforeach
@endif