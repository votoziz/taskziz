@extends('layouts.app')
@section('title', $title ?? 'Мои сообщения')
@section('content')

    <div id="app">
        {{--<example-component></example-component>--}}
        <messages-component  user_id = "{{$user->id}}" kind_of="{{$kind_of}}" csrf="{{ csrf_token() }}"></messages-component>
    </div>

    {{--<br>--}}
    {{--<div class="container">--}}
        {{--<div class="row justify-content-center">--}}
            {{--todo доделать удаление и сделать затем чат--}}
            {{--<div class="col-12">--}}
                {{--<div class="card bg-dim tex ">--}}
                    {{--<div class="card-header text-center font-weight-bold text-white">Сообщения--}}
                        {{--пользователя {{$user->email}}--}}
                    {{--</div>--}}
                    {{--<div class="card-body bg-light pl-5 pr-5">--}}
                        {{--<a href="{{route('messages.create')}}" class="btn btn-success">Новое сообщение</a>--}}
                        {{--<a class="btn bg-dim" href="{{route('messages.index',[$user->id,'received'])}}">--}}
                            {{--Входящие--}}
                            {{--@if($received_new_count > 0)--}}
                                {{--<span class="badge badge-info">{{$received_new_count}}</span>--}}
                            {{--@endif--}}
                        {{--</a>--}}
                        {{--<a class="btn bg-dim" href="{{route('messages.index',[$user->id,'sent'])}}">--}}
                            {{--Исходящие--}}
                        {{--</a>--}}
                        {{--<a class="btn bg-dim" href="{{route('messages.index',[$user->id,'draft'])}}">--}}
                            {{--Черновики--}}
                        {{--</a>--}}
                        {{--<div class="row  br-3  mt-1 mb-1">--}}
                            {{--<div class="col-12">--}}
                                {{--<h2>Мои сообщения</h2>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<hr>--}}
                        {{--@include('messages.includes.'.$kind_of)--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<br>--}}

@endsection
