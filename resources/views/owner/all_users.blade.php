@extends('layouts.app')
@section('title', 'Все пользователи')
@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @include('Owner.includes.users_search')


            <div class="col-12">
                <div class="card bg-info ">
                    <div class="card-header text-center font-weight-bold text-white">Все пользователи</div>

                    <div class="card-body bg-dim">

                        <div class="row">
                            @foreach($users as $user)
                                <div class="col-lg-3 col-md-4 col-sm-6 col-12 pb-4">
                                    <div class="card bg-info  ">

                                        <div class="card-header text-center  text-white">

                                            <a class="text-white" href="{{route('profiles.index',$user->id)}}">


                                            {{$user->email}}
                                        @if(is_null($user->email_verified_at))
                                                <i class="fas fa-ban" style="color:#dd0000;"></i>
                                        @else
                                                <i class="fas fa-check" style="color: #00f700"></i>
                                        @endif

                                                </a>
                                        </div>
                                        <div class="card-body bg-light">

                                            @if ($user->id === 1)
                                                <a class="btn bg-success btn-block text-white btn-sm"
                                                   href="">Владелец</a>

                                            @else

                                                <a class="btn  btn-block text-white btn-sm {{ $user->role_id == 2 ? 'bg-success' : 'bg-info'}}"
                                                   href="{{route('change_role',[$user->id, 2,$users->currentPage()])}}"
                                                >Админ</a>
                                                <a class="btn  btn-block text-white btn-sm {{ !is_null($user->role_id) && $user->role_id <= 3 ? 'bg-success' : 'bg-info'}}"
                                                   href="{{route('change_role',[$user->id, 3,$users->currentPage()])}}"
                                                >Модератор</a>
                                                <a class="btn  btn-block text-white btn-sm {{ !is_null($user->role_id) && $user->role_id <= 4 ? 'bg-success' : 'bg-info'}}"
                                                   href="{{route('change_role',[$user->id, 4,$users->currentPage()])}}"
                                                >Пользователь</a>
                                                <a class="btn  btn-block text-white btn-sm {{ is_null($user->role_id) ? 'bg-success' : 'bg-info'}}"
                                                   href="{{route('change_role',[$user->id, 0,$users->currentPage()])}}"
                                                >Гость</a>
                                            @endif


                                        </div>


                                    </div>

                                </div>


                            @endforeach

                            <div class="col-12 pt-5">

                                {{ $users->links() }}


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
