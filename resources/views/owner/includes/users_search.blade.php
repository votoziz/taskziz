
<div class="col-12 pb-4">
    <div class="card bg-info ">
        <div class="card-header text-center font-weight-bold text-white">Поиск пользователей</div>

        <div class="card-body bg-dim">
            {!! Form::open(['route' => ['all_users'], 'method' => 'GET' ]) !!}
            <div class="form-group">
                {!! Form::label('like', 'Почта:') !!}
                {!! Form::text('like', null, array('class'=>'form-control')) !!}
            </div>

            <div class="row">
                <div class="col-6">
                    {!! Form::submit( 'Поиск',  ["class" =>  "btn btn-success btn-block "]) !!}
                </div>
                <div class="col-6">
                    <a class="btn  btn-info btn-block" href="{{route('all_users')}}">
                        Сбросить</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

    </div>


</div>