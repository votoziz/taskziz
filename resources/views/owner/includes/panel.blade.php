<div class="col-md-12 pb-5 bt-5">
    <div class="card bg-info">
        <div class="card-header text-center font-weight-bold text-white">Панель владельца</div>

        <div class="card-body bg-light">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12 pb-4">
                    <a class="btn bg-dim btn-block" href="{{route('all_users')}}">
                        <i class="fas fa-users"></i>
                        Все пользователи</a>
                </div>

                <div class="col-lg-4 col-md-6 col-12  pb-4">
                    <a class="btn  bg-dim btn-block" href="">Здесь будут все основные команды</a>
                </div>

                <div class="col-lg-4 col-md-6 col-12  pb-4">
                    <a class="btn  bg-dim btn-block" href="">Здесь будет ещё что-то</a>
                </div>
            </div>
        </div>
    </div>
</div>