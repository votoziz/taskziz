@extends('layouts.app')
@section('title', 'Вход')


@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-6 ">
            <div class="card bg-light text-dark ">

                <h3 class="card-header">Вход</h3>

                <div class="card-body">


                    {!! Form::open(["route" => "login"]) !!}
                    <div class="form-group">

                        {!! Form::text("email",null, ["class" => "form-control", "placeholder" => "E-mail"]) !!}
                        {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger log-error">:message</p>') : '') !!}
                    </div>
                    <div class="form-group">
                        {!! Form::password("password", ["class" => "form-control", "placeholder" => "Пароль"]) !!}
                        {!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger log-error">:message</p>') : '') !!}
                    </div>


                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" value="true" name="remember"
                                   id="remember">
                            <label class="custom-control-label" for="remember"> Запомнить меня</label>
                        </div>
                    </div>


                    {!! Form::submit( "Войти" , ["class" => "btn btn-outline-dark btn-lg btn-block"]) !!}
                    {{ Form::close() }}
                    <br>

                    @if (Route::has('password.request'))


                        <a class="text-dark" href="{{route('password.request')}}">Забыли пароль?</a> <br>
                    @endif


                    {{--<a class="text-white" href="{{route('auth.activation.request')}}">Не пришло письмо с активацией?</a>--}}
                    <br>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
