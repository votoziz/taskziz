@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card  bg-danger ">
                <div class="card-header text-white">{{ __('Подтвердите адрес своей почты') }}</div>

                <div class="card-body bg-light-dim">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __(' Вы послали новый запрос активации аккаунта') }}
                        </div>
                    @endif

                    {{ __(' Перед тем как использовать сервис, пожалуйста, проверьте свою почту на ссылку для активации') }}
                    {{ __('Если вам не пришла ссылка для активации') }}, <a href="{{ route('verification.resend') }}">{{ __('нажмите сюда') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
