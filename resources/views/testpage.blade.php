@extends('layouts.app')
@section('title', 'Тестовая')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card bg-light">
                    <div class="card-header">Тестовая</div>

                    <div class="card-body">

                        @if($is_owner)
                            Я владелец
                        {{$test}}
                            <br>
                        @endif
                        Это главная страница
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
