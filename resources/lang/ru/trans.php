<?php
return [

    //ОБЩИЕ
    'name'=> 'Название',


    // return HomeController, PollsController
    'administrator_created'=> 'Администратор создан',
    'user_deleted' => 'Пользователь удален',
    'poll_created' => 'Опрос создан',
    'poll_deleted' => 'Опрос удален',
    'language' => 'Язык',
    'question_deleted' =>'Вопрос удален',
    'question_published' => 'Вопрос опубликован',
    'poll_changed' => 'Опрос изменен',
    'num_changed' => 'Нумерация исправлена',
    'all_qu_published' => 'Все вопросы опубликованы',
    'qu_created' => 'Вопрос создан',
    'qu_hidden' => 'Вопрос скрыт',
    'qu_changed' => 'Вопрос изменен',
    'poll_activated' => 'Опрос активирован',
    'poll_completed' => 'Опрос Завершен',

    // return InvitesController.php

    'invite_deleted' => 'Праглашение удалено',
    'count_ended' => 'У данного приглашения закончились попытки',
    'no_such_invite' => 'Такого приглашения не существует',
    'no_such_access_code' => 'Такого Кода доступа не существует',
    'count_access_ended' => 'У данного кода доступа законочились попытки по данному опросу',
    'poll_not_published' => 'Опрос не активирован',
    'thanks_for_answer' => 'Спасибо что ответили на опрос',

    //404
    '404_text'=>'Страница не найдена',
    /*
     * Меню
    */
    'home'   => 'Главная',
    'your_polls' => 'Ваши опросы',
    'add_new_poll' => 'Добавить новый опрос',
    'search' => 'Поиск',
    'back' => 'Назад',

//    newpoll
    'new_poll' => 'Новый опрос',

   // Form
    'polls' => 'Опросы',
    'poll_name' => 'Название опроса',
    'lang' => 'Язык опроса',
    'select_lang' => 'Выберите язык',
    'description' => 'Описание',
    'btn_create_poll' => 'Создать опрос',

    //home
    'admin_panel' => 'Панель администратора',
    'create_admin' => 'Создать администратора',
    'all_polls' => 'Все опросы',
    'delete_user' => 'Удалить пользователя',
    'delete_poll' => 'Удалить опрос',
    'all_users' => 'Все пользователи',
    'owner' => 'Владелец',
    'qu_count' => 'Количество вопросв',

    //create_user
    'users' => 'Пользователи',
    'num' => 'Номер',
    'login' => 'Логин',
    'email' => 'Почта',
    'user_status' => 'Статус пользователя',
    'owner' => 'Администратор',

    //delete_user
    'delete' => 'Удалить',
    'delete_admin' => 'Удалить администратора',

    //create_poll
    'poll_type'=>'Тип опроса',
    'status'=>'Статус',
    'creation_data'=>'Дата создания',
    'select_qu_type' => 'Выберите тип вопроса',
    'select_poll_type' => 'Выберите тип опроса',

    //poll_question
    'add_new_qu' => 'Добавить новый вопрос',
    'change' => 'Редактировать',
    'change_status' => 'Изменить статус',
    'auto_num' => 'Упорядочить нумерацию',
    'publish_all' => 'Опубликовать все',
    'new_qu'=> 'Новый вопрос',
    'question'=> 'Вопрос',
    'hide'=> 'Скрыть',
    'select_num'=> 'Укажите порядковый номер',
    'create'=> 'Создать',
    'qu_type'=> 'Тип вопроса',
    'qu_status'=> 'Статус вопроса',
    'variants_of_answers'=> 'Варианты ответов',
    'change_poll'=> 'Редактировать опрос',
    'create_invite' => 'Создать приглашение',
    'create_access_code' => 'Создать код доступа',
    'invites' => 'Приглашения',
    'invites_and_codes' => 'Приглашения и коды доступа',
    'access_codes' => 'Коды доступа',
    'activate' => 'Активировать',
    'complete' => 'Завершить',
    'add_answer_option' => 'Добавить вариант ответа',
    'download_poll_answers' => 'Скачать excel по ОТВЕТАМ ',
    'download_poll_answers_res' => 'Скачать excel по РЕСПОНДЕНТАМ',
    'show_answers' => 'Показать ответы на опрос по приглашению',
    'new_qu_answer' => 'Новый вариант ответа',
    'answer_value' => 'Вес ответа',
    'copy' => 'Копировать',
    'excel_by_invite'=>'Excel по приглашению',
    'page_to_excel_access_codes' => 'Excel по кодам доступа',
    'from'=>'с',
    'to'=>'по',
    'download'=>'Скачать',
    'respondents'=>'респондентов',
    'out_of' => 'из',
    'delete_answers' => 'Удалить ответы',

    'poll_question_mark1' => 'Чтобы опрос стал работать необходимо: 1. Активировать опрос. 2. Опубликовать все или необходимые вопросы опроса. 3. Создать приглашение (для того чтобы опрашиваемые могли переходить по нему и отвечать на данный опрос)',

    //change_poll
    'changing' => 'Изменение',
    'change_qu_answer' => 'Изменить вариант ответа',
    'clear_answers' => 'Очистить варианты ответов',
    'change_question_mark1' => 'Для удаления всех вариантов ответов просто нажмите кнопку Очистить варианты ответов затем кнопку Редактировать',

    //create_invite
    'invite_created' => 'Приглашение создано',
    'access_code_created'=> 'Код доступа создан',
    'new_invite' => 'Новое приглашение',
    'name_invite' => 'Автоматически созданное имя',
    'description_invite' => 'Описание приглашения',
    'count' => 'Количество использований',
    'create_invite_mark1' => 'Если не указывать число использований инвайта, он будет действовать бесконечно',

    //create_access_code

    'create_access_code_mark1' => 'Если не указывать число использований кода, он будет действовать бесконечно',
    'new_access_code' => 'Новый код доступа',

    //by_invite
    'other' => 'Другое',
    'why_chosen' => 'Почему вы выбрали именно этот вариант ответа?',
    'yes' => 'Да',
    'no' => 'Нет',
    'next' => 'Дальше',
    'reply' => 'Ответить',
    'complete_survey' => 'Завершить опрос',

    //poll_answers
    'answers_poll' => 'Ответы на опрос',
    'respondent' => 'Опрашиваемый',
    'answer_data' => 'Дата ответа',
    'invite' => 'Приглашение',
    'show' => 'Показать',

    //respondent_answer
    'answer_respondent' => 'Ответы опрашиваемого',
    'text_answer' => 'Текстовый ответ',
    'choisen_answers' => 'Выбранные варианты ответов',







];